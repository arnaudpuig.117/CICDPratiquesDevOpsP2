## Build app :
pip install -r requirements.txt

## Create database
python3 manage.py migrate

## Generate fixtures:
python manage.py loaddata fixtures.json

## Unit tests
python manage.py test

## Functional tests (end to end tests)
python3 -m pytest e2e.py

## Run app:
python manage.py runserver 0.0.0.0:8000


